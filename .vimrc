" install the plugins with
" git clone https://github.com/gmarik/Vundle.vim.git
" ~/.vim/bundle/Vundle.vim
" then run :PluginInstall


set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
" call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
" check syntax
Plugin 'nvie/vim-flake8'
Plugin 'scrooloose/syntastic'
" git integration
Plugin 'tpope/vim-fugitive' 
let python_highlight_all=1
" Add all your plugins here (note older versions of Vundle used Bundle
" instead of Plugin)

Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
" super search with ctrl-p
Plugin 'kien/ctrlp.vim'

Plugin 'rust-lang/rust.vim'

Plugin 'editorconfig/editorconfig-vim'

set clipboard=unnamed

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" javascript syntax
Plugin 'jelera/vim-javascript-syntax'

" Enable folding 
Plugin 'tmhedberg/SimpylFold'
set foldmethod=indent
set foldlevel=99

" file browsing
Plugin 'scrooloose/nerdtree'

" typescript syntax 

Plugin 'leafgarland/typescript-vim'



" Enable folding with the spacebar
nnoremap <space> za

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" nerdtree keybinding

nnoremap <C-g> :NERDTree<CR>
set nu
map Q <ESC>

set expandtab " enter spaces when tab is pressed
set tabstop=4 " use 4 spaces to represent tab
set softtabstop=4
set shiftwidth=4 " number of spaces for auto indent
set autoindent
set backspace=indent,eol,start " make backspaces more usefull

syntax on
 
  
   
   " execute pathogen#infect()
   syntax on
   ""filetype plugin ident on   
